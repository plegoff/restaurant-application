export default {
  getAllProducts(){
    return fetch('http://not-public/getAllProducts')
    .then(response => response.json())
    .catch (error => {
      console.error(error);
    });
  },
  updateProduct(editedItem){
    fetch('http://not-public/updateProduct', {
    method: 'PATCH',
    headers: {
      'Content-type': 'application/json'
    },
    body: editedItem
    })
    .then(console.log(editedItem))
  }
}
