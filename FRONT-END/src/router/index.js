import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import store from "@/store/store";
import goTo from "vuetify/es5/services/goto";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/menu-midi",
    name: "Menu-midi",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "menu-midi" */ "../views/Menu-midi.vue")
  },
  {
    path: "/menu-apres-midi",
    name: "Menu-apres-midi",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(
        /* webpackChunkName: "menu-apres-midi" */ "../views/Menu-apres-midi.vue"
      )
  },
  {
    path: "/a-emporter",
    name: "A-emporter",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "a-emporter" */ "../views/A-emporter.vue")
  },
  {
    path: "/mesures-covid-19",
    name: "Mesures-covid-19",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(
        /* webpackChunkName: "mesures-covid-19" */ "../views/Mesures-covid-19.vue"
      )
  },
  {
    path: "/galerie",
    name: "Galerie",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "galerie" */ "../views/Galerie.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/produits",
    name: "Produits",
    component: () =>
      import(/* webpackChunkName: "produits" */ "../views/Produits.vue"),
    meta: { authRequired: true }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior: (to, from, savedPosition) => {
    let scrollTo = 0
    if (to.hash) {
      scrollTo = to.hash
    } else if (savedPosition) {
      scrollTo = savedPosition.y
    }
    return goTo(scrollTo)
  },
});

/** blocks access to auth required routes (if not authenticated) */
router.beforeEach((to, _, next) => {
  if (to.matched.some(routeRecord => routeRecord.meta.authRequired)) {
    if (!store.state.user) {
      next({
        path: "/",
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
