import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import * as firebase from "firebase/app";

import "firebase/auth";
import store from "./store/store";
import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;
//FIREBASE authentication config
const firebaseConfig = {
  apiKey: "not-public",
  authDomain: "not-public",
  databaseURL: "not-public",
  projectId: "not-public",
  storageBucket: "not-public",
  messagingSenderId: "not-public",
  appId: "not-public"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(user => {
  store.commit("updateUser", { user });
});

new Vue({
  router,
  render: h => h(App),
  vuetify,
  store: store
}).$mount("#app");
