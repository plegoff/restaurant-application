const express = require('express');
const app = express();
const cors = require('cors');
const dotenv = require('dotenv');
const { response } = require('express');

const getConnection = require('./MariaDBConnection.js')

app.use(cors());
/* to send data in json format */
app.use(express.json());
app.use(express.urlencoded({ extended : false }));
dotenv.config();

/* ROUTES */

// PRODUCTS API ROUTES:

// create
app.post('/insertProduct', (request, response) => {
  const { name } = request.body;
  const { description } = request.body;
  const { price } = request.body;
  const { activated } = request.body;
  const { position } = request.body;
  const { categ } = request.body;
  const { img_id } = request.body;
  const { menu } = request.body;

  console.log(request.body);

  const db = getConnection.getMDBCInstance();

  const result = db.insertNewProduct(name, description, price, activated, position, categ, img_id, menu);

  result
  .then(data => response.json({ data : data }))
  .catch((err => console.log(err)));
});


// read
app.get('/getAllProducts', (request, response) => {
  const db = getConnection.getMDBCInstance();
  const result = db.getAllProductsData();

  result
  .then(data => response.json({ data : data }))
  .catch(err => console.log(err));
  
});

// update
app.patch('/updateProduct', (request,response) => {
  console.log(request.body);
  const { id, name, description, price, activated, position, categ, img_id, menu } = request.body;
  const db = getConnection.getMDBCInstance();

  const result = db.updateProductNameById(id, name, description, price, activated, position, categ, img_id, menu);

  result
  .then(data => response.json({ success : data }))
  .catch(err => console.log(err));
});


// delete
app.delete('/deleteProduct/:id', (request, response) => {
  const { id } = request.params;
  const db = getConnection.getMDBCInstance();

  const result = db.deleteProductRowById(id);

  result
  .then(data => response.json({ success : data }))
  .catch(err => console.log(err));
});

// search
app.get('/searchProduct/:name', (request, response) => {
  const { name } = request.params;
  const db = getConnection.getMDBCInstance();

  const result = db.searchProductByName(name);

  result
  .then(data => response.json({ data : data }))
  .catch(err => console.log(err));
})

app.listen(process.env.PORT, () => console.log(`SERVER APP is running on port ${process.env.PORT}`));
