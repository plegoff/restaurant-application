/**
 * CREATES SQL CONNECTION
 * CRUD METHODS on products table + users table
 */
const mysql = require('mysql');
const dotenv = require('dotenv');
dotenv.config();
let instance = null;

// CONNECTION
/**connects to mariaDB database */
function getConnection(){
    return mysql.createConnection({
        host: process.env.HOST,
        port: process.env.DB_PORT,
        user: 'not-public',
        password: process.env.PASSWORD,
        database: process.env.DATABASE,
    })
}


const connection = getConnection()

connection.connect((err) => {
    if (err) {
      console.log(err.message);
    }
    console.log('db '+ connection.state);
  })

/** Contains static function to instanciate a connection to database */
class MariaDBConnection {
    /* static allows to call the function even if no object of the class is initialized */
    static getMDBCInstance() {
        /* if MariaDBConnection does not exist -> creates new connection */
        return instance ? instance : new MariaDBConnection();
    }

    /* PRODUCTS TABLE CRUD FUNCTIONS */

    /**  Gets all products from products table 
    *
    *  data fetched : prod_id, name, description, price, activated, position, categ, img_id, menu
    */
    async getAllProductsData() {
        try {
            const response = await new Promise((resolve, reject) => {
                const query = "SELECT * FROM products;";

                connection.query(query, (err, results) => {
                    if (err) reject(new Error(err.message));
                    resolve(results);
                })
            });

            return response;

        } catch (error) {
            console.log(error);
        }
    }

    /* inserts a new product in products database: params: all products attributes except id which is auto created in SQL database */
    async insertNewProduct(name, description, price, activated, position, categ, img_id, menu) {
        try {
            console.log('dans mariadbconnection.js dans insertNewProduct ='+name, description, price, activated, position, categ, img_id, menu);
            //parse data to be conform to SQL table
            price = parseFloat(price, 10);
            activated = JSON.parse(activated);
            position = parseFloat(position, 10);
            categ = parseInt(categ, 10);
            img_id = parseInt(img_id, 10);
            menu = parseInt(menu, 10);
                
            const response = await new Promise((resolve, reject) => {
                const query = "INSERT INTO products (name, description, price, activated, position, categ, img_id, menu) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

                connection.query(query, [name, description, price, activated, position, categ, img_id, menu], (err, result) => {

                if (err) reject(new Error(err.message));
                })
            });
                
                return response === 1 ? true : false;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    /* deletes full product row / param: id */
    async deleteProductRowById(id) {
        try {
            id = parseInt(id, 10);
            const response = await new Promise((resolve, reject) => {
                const query = "DELETE FROM products WHERE prod_id = ?;";

                connection.query(query, [id], (err, result) => {
                    if (err) reject(new Error(err.message));
                    resolve(result.affectedRows);
                })
            });

            return response === 1 ? true : false;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    /** Updates product row in product table */
    async updateProductNameById(id, name, description, price, activated, position, categ, img_id, menu) {
        try {
            console.log('dans mariadbconnection.js dans updateProductNameById = id ='+id, name, description, price, activated, position, categ, img_id, menu);
            id = parseInt(id, 10);

            price = parseFloat(price, 10);
            activated = JSON.parse(activated);
            position = parseFloat(position, 10);
            categ = parseInt(categ, 10);
            img_id = parseInt(img_id, 10);
            menu = parseInt(menu, 10);

            const response = await new Promise((resolve, reject) => {
                const query = "UPDATE products SET name = ?, description = ?, price = ?, activated = ?, position = ?, categ = ?, img_id = ?, menu = ? WHERE prod_id = ?;";

                connection.query(query, [name, description, price, activated, position, categ, img_id, menu, id], (err, result) => {
                    if (err) reject(new Error(err.message));
                    resolve(result.affectedRows);
                })
            });
            
            return response === 1 ? true : false;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    /** searches a product by name
    *   fetches all attributes of the product
    *   prepared for app's future evolutions/updates 
    *
    */
    async searchProductByName(name) {
        try {
            const response = await new Promise((resolve, reject) => {
                const query = "SELECT * FROM products WHERE name = ?;";

                connection.query(query, [name], (err, results) => {
                    if (err) reject(new Error(err.message));
                    resolve(results);
                })
            });

            return response;

        } catch (error) {
            console.log(error);
        }

    }

}

module.exports = MariaDBConnection;
