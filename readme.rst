Welcome to Restaurant-Application's documentation!
======================================

Restaurant-Application is a restaurant website displaying menus editable by the restaurant owner.

Getting started
---------------
#todo

Features
--------

- Micro service managing products database
- Manage and records menu products via Website GUI
- Final client display of restaurant website + its menus

Installation
------------

Environments recommendation

Development environment :
We recommend Ubuntu 18.04+, macOS OSX Lion or Windows 10

THE PROJECT IS DIVIDED IN TWO PARTS:

- REST API 
- FRONT-END Website


API-REST dependencies:

( NodeJS environment )

cors 2.8.5 : https://www.npmjs.com/package/cors/v/2.8.5

dotenv 8.2.0: https://www.npmjs.com/package/dotenv

express4.17.1: https://www.npmjs.com/package/express

mysql 2.18.1: https://www.npmjs.com/package/mysql?activeTab=versions



FRONT-END dependencies:

( VueJS environment )

core-js3.6.5: https://www.npmjs.com/package/core-js

es6-promise 4.2.8: https://www.npmjs.com/package/es6-promise/v/4.2.8

firebase 7.16.1: https://firebase.google.com/support/release-notes/js#version_7161_-_july_16_2020

node 14.5.0: https://nodejs.org/dist/v14.5.0/docs/api/

vue 2.6.11: https://www.npmjs.com/package/vue

vue-router 3.2.0: https://www.npmjs.com/package/vue-router/v/3.2.0

vuetify 2.2.11: https://vuetifyjs.com/en/

vuex 3.5.1: https://www.npmjs.com/package/vuex